data "template_file" "user_data" {
  template = "${file("sandbox.tpl")}"
  vars = {
    region = "${var.region}"
  }
}

resource "aws_instance" "sandbox_ubuntu" {
    ami                  = "${var.ami_ubuntu_16_04}"
    iam_instance_profile = "${aws_iam_instance_profile.sandbox_profile.name}"
    key_name             = "${var.key_name}"
    user_data            = "${data.template_file.user_data.rendered}"
    instance_type        = "t2.micro"
    tags {
        Name      = "sandbox_ubuntu",
        Inspector = "installed",
        SSM       = "update"
    }
}
