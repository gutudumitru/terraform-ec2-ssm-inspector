variable "region" {}
variable "key_name" {}
variable "ami_ubuntu_16_04" {}
variable "profile" {
  default = "myaws"
}
