Terraform: 

    t2.micro EC2 instance with Systems State Manager agent and AWS Inspector agent.

Usage:

- Run "terraform init && terraform apply"
- This configuration expects you to have an AWS profile setup in ~/.aws/credentials.
- Update variables in terraform.tfvars
- Uses default VPC and security group.

Resources created:

- ec2 instance - t2.micro - Ubuntu 16.04
- iam roles and instance profiles
- ssm association to update the ssm agent daily


EC2 Configuration:

- The AWS Systems State Manager agent is installed.
- The AWS Inspector agent is also installed.
- A tag to use for inspector is created: 

    Key: Inspector
    Value: yes

- A tag is also created and used to udpate the ssm agent:

    Key: SSM
    Value: update
